package com.example.androidclient;

        import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStreamReader;
        import java.io.PrintWriter;
        import java.net.Socket;

        import android.app.Activity;
        import android.content.Intent;
        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.os.Handler;
        import android.os.Message;
        import android.view.View;
        import android.view.View.OnClickListener;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.TabHost;
        import android.widget.TextView;
        import android.widget.Toast;

public class Main2Activity extends Activity {

    private static TextView textResponse;
    private static TextView txtDo_lech;
    private static TextView txtHuong;
    private static TextView txtKhau;
    private static TextView text;
    //Khai bao bien cua phan tu ban
    private static TextView txtGocTam;
    private static TextView txtDoTa;
    private static TextView txtDoHuong;
    private static TextView txtGocTamLenPhao;
    private static TextView txtDoTaLenPhao;
    private static TextView txtHuongLenPhao;
    //Khai bao bien cua luong sua rieng
    private static TextView txtDgt;
    private static TextView txtDn;
    private static TextView txtDVoF;
    private static TextView txtDbs;
    private static TextView txtDbh;
    private static TextView txtHdn;


    private EditText editTextAddress, editTextPort;
    private Button buttonConnect;
    private String message = "Hi client!";
    private static String kq = "";
    private  static  String chuoiguiden ="" ;
    private ClientTask myClientTask;
    private OnListener listener;
    private static boolean flag = true;
    private TabHost tabHost;
    Socket socket = null;
    Intent myIntent = null;
    public interface OnListener {
        void listener(String text);
    }

    public void addListener(OnListener listener) {
        this.listener = listener;
    }

    static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (flag) {
                kq += msg.obj.toString() + "\r\n";
               // textResponse.setText(kq);

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main2);

        //Hien thi phan tab xem
        tabHost = (TabHost) findViewById(R.id.tabHost) ;
        tabHost.setup();

        TabHost.TabSpec spec1 = tabHost.newTabSpec("tab 1");
        spec1.setContent(R.id.tab1);
        spec1.setIndicator("Phần tử bắn");

        TabHost.TabSpec spec2 = tabHost.newTabSpec("tab 2");
        spec2.setContent(R.id.tab2);
        spec2.setIndicator("Lượng sửa riêng");



        tabHost.addTab(spec1);
        tabHost.addTab(spec2);


        editTextAddress = (EditText) findViewById(R.id.address);
        editTextPort = (EditText) findViewById(R.id.port);
        buttonConnect = (Button) findViewById(R.id.connect);

        //txtDo_lech = (TextView) findViewById(R.id.txtdo_lech);
        //txtHuong = (TextView)  findViewById(R.id.txthuong);
        //txtKhau = (TextView) findViewById(R.id.txtkhau);

        //lay text phan phan tu ban
        txtGocTam= (TextView) findViewById(R.id.txtGocTam);
        txtDoTa= (TextView) findViewById(R.id.txtDoTa);
        txtDoHuong= (TextView) findViewById(R.id.txtDoHuong);
        txtGocTamLenPhao= (TextView) findViewById(R.id.txtGocTamLenPhao);
        txtDoTaLenPhao= (TextView) findViewById(R.id.txtDoTaLenPhao);
        txtHuongLenPhao= (TextView) findViewById(R.id.txtHuongLenPhao);


        //lay text phan luong sua rieng
        txtDgt= (TextView) findViewById(R.id.txtDgt);
        txtDn= (TextView) findViewById(R.id.txtDn);
        txtDVoF= (TextView) findViewById(R.id.txtDVoF);
        txtDbs= (TextView) findViewById(R.id.txtDbs);
        txtDbh= (TextView) findViewById(R.id.txtDbh);
        txtHdn= (TextView) findViewById(R.id.txtHdn);
        //Lay hai tham so tu ngoai gui vao
        final String address = getIntent().getStringExtra("address");
        final String port = getIntent().getStringExtra("port");

                // TODO Auto-generated method stub
                myClientTask = new ClientTask(address
                        .toString(), Integer.parseInt(port));//truyen hai tham so vao de ket noi
                myClientTask.execute();//Thuwc thi ket noi den server

    }

    public class ClientTask extends AsyncTask<String, String, String> implements
            OnListener {

        String dstAddress;
        int dstPort;
        PrintWriter out1;


        ClientTask(String addr, int port) {
            dstAddress = addr;
            dstPort = port;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {

                socket = new Socket(dstAddress, dstPort);
                out1 = new PrintWriter(socket.getOutputStream(), true);
                //out1.print("Hello server!");
                out1.flush();

                BufferedReader in1 = new BufferedReader(new InputStreamReader(
                        socket.getInputStream()));
                do {
                    try {
                        if (!in1.ready()) {
                            if (message != null) {
                                MainActivity.handler.obtainMessage(0, 0, -1,
                                        "Server: " + message).sendToTarget();
                                message = "";
                                String deviceName = android.os.Build.MODEL;
                                sendMessage(deviceName);

                            }
                        }
                        int num = in1.read();
                        message += Character.toString((char) num);

//Phan tich thong tin gui toi cho dt
                        chuoiguiden = message;

                        String[] phantichchuoi = chuoiguiden.split(";");
                        String phantuban = phantichchuoi[0];
                        String luongsuarieng = phantichchuoi[1];
                        String[] separated = phantuban.split(":");
                        //txtDo_lech.setText(separated[0]);
                        //txtHuong.setText(separated[1]);
                        //txtKhau.setText(separated[2]);
                        //Phan tu  ban
                        txtGocTam.setText(separated[0]);
                        txtDoTa.setText(separated[1]);
                        txtDoHuong.setText(separated[2]);
                        txtGocTamLenPhao.setText(separated[3]);
                        txtDoTaLenPhao.setText(separated[4]);
                        txtHuongLenPhao.setText(separated[5]);
                        //Luong sua rieng
                        separated = luongsuarieng.split(":");
                        txtDgt.setText(separated[0]);
                        txtDn.setText(separated[1]);
                        txtDVoF.setText(separated[2]);
                        txtDbs.setText(separated[3]);
                        txtDbh.setText(separated[4]);
                        txtHdn.setText(separated[5]);
                    } catch (Exception classNot) {
                    }

                } while (!message.equals("bye"));

                try {sendMessage("bye");
                } catch (Exception e) {

                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } finally {
                    socket.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (socket.isClosed()) {
                    flag = false;
                }
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Lỗi nhập!", Toast.LENGTH_LONG).show();
            }

            super.onPostExecute(result);
        }

        @Override
        public void listener(String text) {
            // TODO Auto-generated method stub
            sendMessage(text);
        }

        void sendMessage(String msg) {
            try {
                out1.print(msg);
                out1.flush();
                if (!msg.equals("bye"))
                    MainActivity.handler.obtainMessage(0, 0, -1, "Me: " + msg)
                            .sendToTarget();
                else
                    MainActivity.handler.obtainMessage(0, 0, -1,
                            "Ngắt kết nối!").sendToTarget();
            } catch (Exception ioException) {
                ioException.printStackTrace();
            }
        }

    }

    public void send(View v) {
        addListener(myClientTask);
       // if (listener != null)
            //listener.listener(((EditText) findViewById(R.id.editText1))
             //       .getText().toString());
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        try {
            if (listener != null)
                listener.listener("bye");
            socket.close();
        } catch (Exception e) {
            // TODO: handle exception
        }
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        try {
            if (listener != null)
                listener.listener("bye");
            socket.close();
        } catch (Exception e) {
            // TODO: handle exception
        }
        super.onStop();
    }

    public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

}
