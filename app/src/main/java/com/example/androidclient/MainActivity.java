package com.example.androidclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private EditText editTextAddress, editTextPort;
	private Button buttonConnect;
	private String message = "Hi client!";
	private static String kq = "";
	private  static  String chuoiguiden ="" ;

	private OnListener listener;
	private static boolean flag = true;
	private TabHost tabHost;
    Socket socket = null;
	Intent myIntent = null;
	public interface OnListener {
		void listener(String text);
	}

	public void addListener(OnListener listener) {
		this.listener = listener;
	}

	static Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (flag) {
				kq += msg.obj.toString() + "\r\n";
				//textResponse.setText(kq);

			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);


		 editTextAddress = (EditText) findViewById(R.id.address);
		editTextPort = (EditText) findViewById(R.id.port);
		buttonConnect = (Button) findViewById(R.id.connect);

		buttonConnect.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				myIntent = new Intent(MainActivity.this, Main2Activity.class);
				myIntent.putExtra("address",editTextAddress.getText().toString());
				myIntent.putExtra("port",editTextPort.getText().toString());
				startActivityForResult(myIntent,0);
			}
		});


	}



	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		try {
			if (listener != null)
				listener.listener("bye");
			socket.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onDestroy();
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		try {
			if (listener != null)
				listener.listener("bye");
			socket.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onStop();
	}
	
	public void onClick(View v) {
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		startActivity(intent);
		finish();
	}

}
