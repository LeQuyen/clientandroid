package com.example.androidclient;

import android.os.Bundle;
import android.app.Activity;
import android.widget.TextView;

public class Main3Activity extends Activity {
TextView text1 ;
    TextView text2 ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        //Lay hai tham so tu ngoai gui vao
         String address = getIntent().getStringExtra("address");
         String port = getIntent().getStringExtra("port");
        text1 = findViewById(R.id.textView);
        text2 = findViewById(R.id.textView2);
        text1.setText(address);
        text2.setText(port);
    }

}
